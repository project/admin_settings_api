<?php

namespace Drupal\site_settings_example;

use Drupal\admin_settings_api\AdminSettingsAPIService;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Example configuration override.
 */
class ConfigOverrides implements ConfigFactoryOverrideInterface {

  /**
   * @var \Drupal\admin_settings_api\AdminSettingsAPIService
   */
  protected $settingsHelper;

  public function __construct(AdminSettingsAPIService $settingsHelper) {
    $this->settingsHelper = $settingsHelper;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    $overrides = array();
    if (in_array('system.site', $names)) {
      $overrides['system.site'] = [
        'name' => $this->settingsHelper->getSetting('site_settings_example_site_name', 'Example site'),
        'mail' => $this->settingsHelper->getSetting('site_settings_example_site_mail', 'test@example.com'),
      ];
    }
    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'SiteSettingsExample';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
