<?php

namespace Drupal\site_settings_example\Plugin\AdminSettingsAPI;


use Drupal\Core\Annotation\Translation;
use Drupal\admin_settings_api\Annotation\AdminSettingsAPI;
use Drupal\admin_settings_api\Plugin\AdminSettingsAPIBase;

/**
 * Adds component to admin form
 *
 * @AdminSettingsAPI(
 *   id = "site_settings_example",
 *   label = @Translation("Site settings example"),
 * )
 */
class SiteSettings extends AdminSettingsAPIBase {

  /**
   * The form group label
   */
  public function label() {
    return $this->t('Site settings example');
  }

  /**
   * Build the form component.
   */
  public function build() {
    $formComponent = [];

    $formComponent['site_settings_example_site_name'] = [
      '#type' => 'textfield',
      '#title' => t('Site name'),
      '#storage_type' => 'state',
      '#default_value' => $this->getDefaultValue('site_settings_example_site_name'),
    ];

    $formComponent['site_settings_example_site_mail'] = [
      '#type' => 'email',
      '#title' => t('Site email'),
      '#storage_type' => 'state',
      '#default_value' => $this->getDefaultValue('site_settings_example_site_mail'),
    ];

    return $formComponent;
  }
}
