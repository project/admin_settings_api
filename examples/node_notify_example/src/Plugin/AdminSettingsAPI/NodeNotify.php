<?php

namespace Drupal\node_notify_example\Plugin\AdminSettingsAPI;


use Drupal\Core\Annotation\Translation;
use Drupal\admin_settings_api\Annotation\AdminSettingsAPI;
use Drupal\admin_settings_api\Plugin\AdminSettingsAPIBase;

/**
 * Adds component to admin form
 *
 * @AdminSettingsAPI(
 *   id = "node_notify_example",
 *   label = @Translation("Node notify example"),
 * )
 */
class NodeNotify extends AdminSettingsAPIBase {

  const
    EMAIL = 'node_notify_example_site_mail',
    SUBJECT = 'node_notify_example_site_subject',
    MESSAGE = 'node_notify_example_site_message';

  /**
   * The form group label
   */
  public function label() {
    return $this->t('Node notify example');
  }

  /**
   * Build the form component.
   */
  public function build() {
    $formComponent = [];

    $formComponent['help'] = [
      '#type' => 'markup',
      '#markup' => t('Configuration for email notifications when new nodes are created'),
    ];

    $formComponent[self::EMAIL] = [
      '#type' => 'email',
      '#title' => t('Email'),
      '#storage_type' => 'config',
      '#default_value' => $this->getDefaultValue(self::EMAIL),
    ];

    $formComponent[self::SUBJECT] = [
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#storage_type' => 'config',
      '#default_value' => $this->getDefaultValue(self::SUBJECT),
    ];

    $formComponent[self::MESSAGE] = [
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#storage_type' => 'config',
      '#default_value' => $this->getDefaultValue(self::MESSAGE),
    ];

    $formComponent['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['node'],
      '#show_restricted' => TRUE,
    ];

    return $formComponent;
  }
}
